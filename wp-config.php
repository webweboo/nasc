<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'nasc' );
//define( 'DB_NAME', 'weboodig_nasc' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );
//define( 'DB_USER', 'weboodig_nasc' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );
//define( 'DB_PASSWORD', 'w1GnYO7p_3bK' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J>[*4;,l3d#Z:DxXVh]mSzNy^&ZX;6o83qRwF[0i^<nD%XXmg7<82buV`yOY&z%{' );
define( 'SECURE_AUTH_KEY',  'f)lYB|S&J4baVYYE%6Nf~`[hzaF=eE8)1.$+pr[N_{YMHIa(D/gekSp,$2~;6{`K' );
define( 'LOGGED_IN_KEY',    'iS><N]%7:e,gIyQ 9jV`~`H=LcXfGO*hvB`3J3ZhGht7q&*gK==x}k!x3*<h`ML@' );
define( 'NONCE_KEY',        'O<r-@+x)pEJEl2QwU3*b&I! kW+2Y^ypWI~LB+l5W=rk(pzQWTVCL29F)mw605~9' );
define( 'AUTH_SALT',        '=XoRGf_75??wHqu^@nsn]Ab7Z&pSk!zLV`QYL!]-?3v@_(8_`c0:$DINAy gBspl' );
define( 'SECURE_AUTH_SALT', 'm`Hw]S{bSf^jY3<x7E3 #uq[vEF4uxk5Z[`heXw`}6oC8u$YR,l:uT45I0#vd6tf' );
define( 'LOGGED_IN_SALT',   'W|cz1UV$@x~htmU?CFfYu,t4%-`[8LLno;@!v,m}^dd#nk59NB-[~P&pqDjW2RS3' );
define( 'NONCE_SALT',       '$8FZcV}FeZ0.69*,HI{Y _D*+iK?`1yQqVk>t)lv v^56CwzgPj/`=(d#q}GFcg:' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'ns_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
