    <footer>
      <div class="container">
        <div class="row">
          <div class="col-sm-4 block-footer footer-block-1">
            <?php dynamic_sidebar( 'rodape-1' ); ?>
          </div>
          <!-- /Logo Footer & Sobre -->
          
          <div class="col-sm-3 offset-sm-1 block-footer footer-block-2">
            <?php dynamic_sidebar( 'rodape-2' ); ?>
          </div>
          <!-- /Dados Contato -->

          <div class="col-sm-3 offset-sm-1 block-footer footer-block-3">
            <?php dynamic_sidebar( 'rodape-3' ); ?>
          </div>
          <!-- /Localização -->
        </div>
      </div>
    </footer>    

    <?php wp_footer() ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>
