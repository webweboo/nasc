<?php require 'head.php' ?>

  <body <?php body_class();?>>
    
    <header>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3 col-lg-2">
            <a class="logotipoHeader" href="<?php echo get_permalink(82); ?>" title="NASC - Comercial Internacional">
              <img src="<?php bloginfo('template_url'); ?>/images/nasc_comercio_internacional.svg" alt="NASC - Comercial Internacional">
            </a>
          </div>
          <!-- /Logotipo -->

          <div class="col-md-9 col-lg-10 align-self-center">
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
          </div>
          <!-- /menu - nav -->
        </div>
      </div>
    </header>