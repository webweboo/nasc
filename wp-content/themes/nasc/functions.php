<?php

function wp_action_enqueue_scripts() {
  wp_enqueue_style( 'style', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'wp_action_enqueue_scripts' );
/* Adicionando css dinamicamente. */

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}

add_action( 'init', 'register_my_menus' );
/* Menu dinâmico. */


function widgets_novos_widgets_init() {

	register_sidebar( array(
		'name' => 'Logo & Texto',
		'id' => 'rodape-1',
		'before_widget' => '<div>',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
    'after_title' => '</h4>',
    'description' => __( 'Rodapé', 'text_domain' ),
  ));
  
  register_sidebar( array(
    'name'        => 'Contato',
    'id'          => 'rodape-2',
    'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="dados-footer dados-footer-1 icon phone">',
		'after_title'   => '</h4>',
    'description' => __( 'Rodapé', 'text_domain' ),
  ) );
  
  register_sidebar( array(
    'name'        => 'Localização',
    'id'          => 'rodape-3',
    'before_widget' => '<div>',
		'after_widget'  => '</div>',
    'before_title'  => '<h4 class="dados-footer dados-footer-2 icon localization">',
		'after_title'   => '</h4>',
    'description' => __( 'Rodapé', 'text_domain' ),
  ) );

}

add_action( 'widgets_init', 'widgets_novos_widgets_init' );
/* Widgets [Utilizado no rodapé] */
